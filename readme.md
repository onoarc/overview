## L5 Blog

[Laravel5でシンプルなCRUDアプリを開発する](http://blog.asial.co.jp/article.php?id=1360).

### インストール

    git clone git@bitbucket.org:takahashiyuya/l5-blog.git

### 起動

    // パーミッション、データベース設定、マイグレーション、シードを実行した後
    php artisan serve --host="127.0.0.1" &
    open http://127.0.0.1:8000

### License

[MIT license](http://opensource.org/licenses/MIT)
